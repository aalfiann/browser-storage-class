/*! BrowserStorageClass v1.2.0 | (c) 2022 M ABD AZIZ ALFIAN | MIT License | https://gitlab.com/aalfiann/browser-storage-class */
export = BrowserStorageClass;

declare class BrowserStorageClass {
  /**
   * Constructor
   * @param {WindowLocalStorage} _typeStorage   It can be localStorage or sessionStorage
   * @param {boolean} _promisify                If set to true, all methods will return as promise. Default is false.
   */
  constructor(_typeStorage: WindowLocalStorage, _promisify?: boolean);
  /**
   * Make asynchronous process based on promise
   * @param {Function} fn   Function callback
   * @returns {mixed}       Will return promise if _promisify set to true
   */
  _promise(fn: Function): any;
  /**
   * Make List data key and value
   * @returns {array}
   */
  _makeList(): {
      key: string;
      value: any;
  }[];
  /**
   * Has key
   * @param {string} key    Key name
   * @returns {boolean}
   */
  has(key: string): boolean;
  /**
   * Get value
   * @param {string} key    Key name
   * @returns {mixed}
   */
  get(key: string): any;
  /**
   * Set key and value
   * @param {string} key    Key name
   * @param {mixed} value   Array / String / Object
   */
  set(key: string, value: any): any;
  /**
   * Remove single data
   * @param {string} key
   */
  remove(key: string): any;
  /**
   * Clear all saved data key and value
   */
  clear(): any;
  /**
   * List all data key and value
   * @returns {array}
   */
  list(): any;
}

